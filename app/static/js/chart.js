function offsetYears(date, offset) {
    return new Date(date.setFullYear(date.getFullYear() + offset));
}

function loadData(symbol) {
    d3.selectAll("svg").remove();

    d3.json('/daily/' + symbol).then(data => {

        var data = data.map(info => ({
            date: new Date(info.date),
            high: parseFloat(info.high),
            low: parseFloat(info.low),
            open: parseFloat(info.open),
            close: parseFloat(info.close),
            volume: parseInt(info.volume)
        }));

        initialiseChart(data);
    }, error => {
        var chartDiv = document.getElementById('chart')
        var errorDiv = document.getElementById('error')

        errorDiv.innerHTML = "<h1>There was an error retrieving info for " + symbol + "</h1><p>Perhaps the symbol is invalid or was mistyped.</p>"

        chartDiv.style.display = "none";
        errorDiv.style.display = "flex";
    });
};

const movingAverage = (data, numberOfPricePoints) => {
    return data.map((row, index, total) => {
        const start = Math.max(0, index - numberOfPricePoints);
        const end = index;
        const subset = total.slice(start, end + 1);
        const sum = subset.reduce((a, b) => {
            return a + b['close'];
        }, 0);

        return {
            date: row['date'],
            average: sum / subset.length
        };
    });
};

const initialiseChart = data => {
    data = data.filter(
        row => row['high'] && row['low'] && row['close'] && row['open']
    );

    chartStartDate = offsetYears(new Date(), -1);

    // Filter out data more than 1 year old
    data = data.filter(row => {
        if (row['date']) {
            return row['date'] >= chartStartDate;
        }
    });

    const margin = { top: 50, right: 50, bottom: 50, left: 50 };
    const width = 1850 - margin.left - margin.right;
    const height = 750 - margin.top - margin.bottom;

    // find data range
    const xMin = d3.min(data, d => {
        return d['date'];
    });

    const xMax = d3.max(data, d => {
        return d['date'];
    });

    const yMin = d3.min(data, d => {
        return d['close'];
    });

    const yMax = d3.max(data, d => {
        return d['close'];
    });

    // scale using range
    const xScale = d3
        .scaleTime()
        .domain([xMin, xMax])
        .range([0, width]);

    const yScale = d3
        .scaleLinear()
        .domain([yMin - 5, yMax])
        .range([height, 0]);

    // add chart SVG to the page
    const svg = d3
        .select('#chart')
        .append('svg')
        .attr('width', width + margin['left'] + margin['right'])
        .attr('height', height + margin['top'] + margin['bottom'])
        .append('g')
        .attr('transform', `translate(${margin['left']}, ${margin['top']})`);

    // create the axes component
    svg
        .append('g')
        .attr('id', 'xAxis')
        .attr('transform', `translate(0, ${height})`)
        .call(d3.axisBottom(xScale));

    svg
        .append('g')
        .attr('id', 'yAxis')
        .attr('transform', `translate(${width}, 0)`)
        .call(d3.axisRight(yScale));

    // renders close price line chart and moving average line chart

    // generates lines when called
    const line = d3
        .line()
        .x(d => {
            return xScale(d['date']);
        })
        .y(d => {
            return yScale(d['close']);
        });

    const movingAverageLine = d3
        .line()
        .x(d => {
            return xScale(d['date']);
        })
        .y(d => {
            return yScale(d['average']);
        })
        .curve(d3.curveBasis);

    svg
        .append('path')
        .data([data]) // binds data to the line
        .style('fill', 'none')
        .attr('id', 'priceChart')
        .attr('stroke', '#3a9ead')
        .attr('stroke-width', '1.5')
        .attr('d', line);

    // calculates simple moving average over 10 days
    const movingAverageData = movingAverage(data, 9);
    svg
        .append('path')
        .data([movingAverageData])
        .style('fill', 'none')
        .attr('id', 'movingAverageLine')
        .attr('stroke', '#FF8900')
        .attr('d', movingAverageLine);

    // renders x and y crosshair
    const focus = svg
        .append('g')
        .attr('class', 'focus')
        .style('display', 'none');

    focus.append('circle').attr('r', 4.5);
    focus.append('line').classed('x', true);
    focus.append('line').classed('y', true);

    svg
        .append('rect')
        .attr('class', 'overlay')
        .attr('width', width)
        .attr('height', height)
        .on('mouseover', () => focus.style('display', null))
        .on('mouseout', () => focus.style('display', 'none'))
        .on('mousemove', generateCrosshair);

    d3.select('.overlay').style('fill', 'none');
    d3.select('.overlay').style('pointer-events', 'all');

    d3.selectAll('.focus line').style('fill', 'none');
    d3.selectAll('.focus line').style('stroke', '#67809f');
    d3.selectAll('.focus line').style('stroke-width', '1.5px');
    d3.selectAll('.focus line').style('stroke-dasharray', '3 3');

    //returs insertion point
    const bisectDate = d3.bisector(d => d.date).left;

    /* mouseover function to generate crosshair */
    function generateCrosshair() {
        //returns corresponding value from the domain
        const correspondingDate = xScale.invert(d3.mouse(this)[0]);
        //gets insertion point
        const i = bisectDate(data, correspondingDate, 1);
        const d0 = data[i - 1];
        const d1 = data[i];
        const currentPoint =
            correspondingDate - d0['date'] > d1['date'] - correspondingDate ? d1 : d0;
        focus.attr(
            'transform',
            `translate(${xScale(currentPoint['date'])}, ${yScale(
                currentPoint['close']
            )})`
        );

        focus
            .select('line.x')
            .attr('x1', 0)
            .attr('x2', width - xScale(currentPoint['date']))
            .attr('y1', 0)
            .attr('y2', 0);

        focus
            .select('line.y')
            .attr('x1', 0)
            .attr('x2', 0)
            .attr('y1', 0)
            .attr('y2', height - yScale(currentPoint['close']));

        // updates the legend to display the date, open, close, high, low, and volume of the selected mouseover area
        updateLegends(currentPoint);
    }

    /* Legends */
    const updateLegends = currentData => {
        d3.selectAll('.lineLegend').remove();

        const legendKeys = Object.keys(data[0]);
        const lineLegend = svg
            .selectAll('.lineLegend')
            .data(legendKeys)
            .enter()
            .append('g')
            .attr('class', 'lineLegend')
            .attr('transform', (d, i) => {
                return `translate(0, ${i * 20})`;
            });
        lineLegend
            .append('text')
            .text(d => {
                if (d === 'date') {
                    return `${d}: ${currentData[d].toLocaleDateString()}`;
                } else if (
                    d === 'high' ||
                    d === 'low' ||
                    d === 'open' ||
                    d === 'close'
                ) {
                    return `${d}: ${currentData[d].toFixed(2)}`;
                } else {
                    return `${d}: ${currentData[d]}`;
                }
            })
            .style('fill', 'black')
            .style('font-size', '12px')
            .attr('transform', 'translate(15,9)'); //align texts with boxes
    };

    /* Volume series bars */
    const volData = data.filter(d => d['volume'] !== null && d['volume'] !== 0);

    const yMinVolume = d3.min(volData, d => {
        return Math.min(d['volume']);
    });

    const yMaxVolume = d3.max(volData, d => {
        return Math.max(d['volume']);
    });

    const yVolumeScale = d3
        .scaleLinear()
        .domain([yMinVolume, yMaxVolume])
        .range([height, height * (3 / 4)]);

    svg
        .selectAll()
        .data(volData)
        .enter()
        .append('rect')
        .attr('x', d => {
            return xScale(d['date']);
        })
        .attr('y', d => {
            return yVolumeScale(d['volume']);
        })
        .attr('class', 'vol')
        .attr('fill', (d, i) => {
            if (i === 0) {
                return '#03a678';
            } else {
                return volData[i - 1].close > d.close ? '#c0392b' : '#1ca044'; // green bar if price is rising during that period, and red when price  is falling
            }
        })
        .attr('width', 1)
        .attr('height', d => {
            return height - yVolumeScale(d['volume']);
        });
    // testing axis for volume
    /*
    svg.append('g').call(d3.axisLeft(yVolumeScale));
    */
};
