from flask import Flask, request, render_template, jsonify
from datetime import datetime, timedelta
import requests

app = Flask(__name__)

api_key = ''
api_url_base = 'https://www.alphavantage.co/query'
query_cache = {}

def format_data(data):
    data_formatted = []

    for date, info in data['Time Series (Daily)'].items():
        data_formatted.append({
            'date'   : date,
            'open'   : info['1. open'],
            'high'   : info['2. high'],
            'low'    : info['3. low'],
            'close'  : info['4. close'],
            'volume' : info['5. volume']
        })

    data_formatted.reverse()

    return data_formatted

@app.route('/static/<path:path>')
def static_file(path):
    return app.send_static_file(path)

@app.route('/')
def index():
    symbol = request.args.get('symbol')
    return render_template('index.html',symbol=symbol)

@app.route('/daily/<string:symbol>')
def daily(symbol):
    if not symbol in query_cache or (query_cache[symbol]['date'] + timedelta(days=1)) <= datetime.now():
        api_url = '{0}?function=TIME_SERIES_DAILY&symbol={1}&outputsize=full&apikey={2}&datatype=json'.format(api_url_base, symbol, api_key)
        response = requests.get(api_url)

        if response.status_code == 200 and 'Time Series (Daily)' in response.json():
            query_cache[symbol] = {'date': datetime.now(), 'data': format_data(response.json())}
        else:
            query_cache[symbol] = {'date': datetime.now(), 'data': {'error': 'There was an error retrieving the data for {0}'.format(symbol)}}

    if 'error' in query_cache[symbol]['data']:
        return jsonify(query_cache[symbol]['data']), 404
    else:
        return jsonify(query_cache[symbol]['data'])

if __name__ == '__main__':
    app.run(port=8080)