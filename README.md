# StockAnalyser

![Screenshot of the generated graph](docs/screenshot.png)

A quick stock chart generator I put together following the tutorial from [FreeCodeCamp.org - How to build historical price charts with D3.js](https://www.freecodecamp.org/news/how-to-build-historical-price-charts-with-d3-js-72214aaf6ba3/)

## Installation
1. Clone this repo into a Python virtual environment
2. Install the required libraries

       pip install -r requirement.txt
      
3. Register for an account at [Alpha Vantage](<https://www.alphavantage.co/>)
4. Enter your Alpha Vantage API key in app.py
5. Start the virtual environment and run app.py

## Libraries, APIs, languages used:

* Javascript
* Python
* Flask
* D3.js
* Alpha Vantage stock API


## How to read the chart:

* Yellow line indicates the 10 day simple moving average
* Blue line indicates daily closing prices
* Bottom bars indicate trading volume
* Bottom bars are coloured red, or green depending on whether that day's closing price was higher or lower than the day before